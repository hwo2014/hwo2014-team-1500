package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import noobbot.messages.sendmsg.Join;
import noobbot.messages.sendmsg.JoinRace;
import noobbot.models.BotId;

public class Main {
    
    
    public static void startMultiple(String botName, String botKey, int count, String host, int port) throws IOException, InterruptedException {
        ArrayList<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < count; i++) {
            final Socket socket = new Socket(host, port);
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
            ServerSession serverSession;
            if (count == 1) {
                serverSession = new ServerSession(reader, writer, new Join(new BotId(botName, botKey)));//new Join(new BotId(botName, botKey))
            } else {
                serverSession = new ServerSession(reader, writer, new JoinRace(new BotId(botName+i, botKey), count, "france", null));//new Join(new BotId(botName, botKey))
            }
            Thread thread = new Thread(serverSession);
            thread.start();
            threads.add(thread);
            Thread.sleep(500);
        }
        
        for (Thread thread : threads) {
            thread.join();
        }
    }
    
    
    public static void main(String... args) throws IOException, InterruptedException {
        String host;
        int port;
        String botName;
        String botKey;
        int numberOfBots;
        if (args.length == 0) {
            numberOfBots = 6;
            botName = "weequ bot";
            host = "senna.helloworldopen.com";
            port = 8091;
            botKey = "QCX66ZhI27s2Sw";
        } else {
            numberOfBots = 1;
            host = args[0];
            port = Integer.parseInt(args[1]);
            botName = args[2];
            botKey = args[3];
        }

        startMultiple(botName, botKey, numberOfBots, host, port);
        //System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);


        //ArrayList<SendMsg> msgs = new ArrayList<SendMsg>();
        //msgs.add(new CreateRace(new BotId(botName, botKey), "usa", 2, "moi"));
        //msgs.add(new JoinRace(new BotId(botName, botKey), 2, "usa", "moi"));
        
        //ServerSession serverSession = new ServerSession(reader, writer, new CreateRace(new BotId(botName, botKey), "keimola", 2, null));
    }
}



