/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 *
 * @author antti
 */
public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}
