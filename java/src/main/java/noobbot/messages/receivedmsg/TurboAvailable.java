/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.receivedmsg;

/**
 *
 * @author Antti
 */
public class TurboAvailable {
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
    
    public boolean activated = false;
    
    public boolean isOn() {
        return activated && turboDurationTicks > 0;
    }
    
}
