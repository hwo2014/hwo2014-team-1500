/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.messages.receivedmsg;

import noobbot.models.CarPosition;
import java.util.ArrayList;

/**
 *
 * @author antti
 */
public class CarPositions extends ArrayList<CarPosition> {
    
    public CarPosition getCarPosition(CarID carID) {
        for (CarPosition cP: this) {
            if (cP.id.equals(carID)) {
                return cP;
            }
        }
        return null;
    }
}
