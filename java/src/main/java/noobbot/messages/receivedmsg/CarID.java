/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.messages.receivedmsg;

/**
 *
 * @author antti
 */
public class CarID {
    public String name;
    public String color;
    
    public boolean equals(CarID carID) {
        return (name.equals(carID.name) && color.equals(carID.color));
    }
}
