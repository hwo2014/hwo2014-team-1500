/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.messages.sendmsg;

import noobbot.messages.SendMsg;
import noobbot.models.BotId;

/**
 *
 * @author antti
 */
public class Join extends SendMsg {
    BotId botId;
    
    public Join(BotId botId) {
        this.botId = botId;
    }

    @Override
    protected Object msgData() {
        return botId;
    }
    
    @Override
    protected String msgType() {
        return "join";
    }
}
