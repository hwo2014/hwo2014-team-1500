/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.sendmsg;

import noobbot.messages.SendMsg;

/**
 *
 * @author Antti
 */
public class Turbo extends SendMsg {

    String message;
    
    public Turbo(String message) {
        this.message = message;
    }
    
    @Override
    protected Object msgData() {
        return message;
    }
    
    @Override
    protected String msgType() {
        return "turbo";
    }
    
}
