/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.messages.sendmsg;

import noobbot.messages.SendMsg;

/**
 *
 * @author antti
 */
public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
