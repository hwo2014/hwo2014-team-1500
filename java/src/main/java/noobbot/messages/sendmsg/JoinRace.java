/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.sendmsg;

import noobbot.messages.SendMsg;
import noobbot.models.BotId;

/**
 *
 * @author Antti
 */
public class JoinRace extends SendMsg {
    int carCount;
    String trackName;
    BotId botId;
    String password;
    
    public JoinRace(BotId botId, int carCount, String trackName, String password) {
        this.botId = botId;
        this.carCount = carCount;
        this.trackName = trackName;
        this.password = password;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
