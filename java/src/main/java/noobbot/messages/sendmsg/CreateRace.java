/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot.messages.sendmsg;

import noobbot.messages.SendMsg;
import noobbot.models.BotId;

/**
 *
 * @author anttkaik
 */
public class CreateRace extends SendMsg {

    String trackName;
    int carCount;
    BotId botId;
    String password;
    
    public CreateRace(BotId botId, String trackName, int carCount, String password) {
        this.trackName = trackName;
        this.botId = botId;
        this.carCount = carCount;
        this.password = password;
    }
    
    
    @Override
    protected String msgType() {
        return "createRace";
    }
    
}
