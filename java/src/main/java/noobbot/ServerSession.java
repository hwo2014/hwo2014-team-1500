package noobbot;

import noobbot.models.CarPosition;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import noobbot.messages.MsgWrapper;
import noobbot.messages.SendMsg;
import noobbot.messages.receivedmsg.CarID;
import noobbot.messages.receivedmsg.CarPositions;
import noobbot.messages.receivedmsg.GameInit;
import noobbot.messages.receivedmsg.TurboAvailable;
import noobbot.messages.sendmsg.Throttle;
import noobbot.messages.sendmsg.Turbo;
import noobbot.models.LaneMetric;
import noobbot.models.Piece;
import noobbot.models.PiecePosition;
import noobbot.models.util.PhysicalConstants;
import noobbot.models.util.Speeds;
import noobbot.models.util.TrackMetrics;

public class ServerSession implements Runnable {

    private final Gson gson = new Gson();
    private PrintWriter writer;
    private SendMsg join;
    private BufferedReader reader;

    private GameInit gameInit = null;
    private CarID myCar = null;
    private CarPositions currentPositions = null;
    private TrackMetrics trackMetrics = null;
    private Speeds speeds = null;
    private PhysicalConstants physicalConstants = new PhysicalConstants();
    private TurboAvailable turboAvailable = null;
    private PositionsMessageHandler positionsHandler = new PositionsMessageHandler();

    private CarPosition myCarPosition = null;
    private Piece currentPiece = null;
    private LaneMetric currentLane = null;

    public ServerSession(final BufferedReader reader, final PrintWriter writer, SendMsg join) throws IOException {
        this.writer = writer;
        this.join = join;
        this.reader = reader;
    }

    @Override
    public void run() {
        try {
            String line = null;
            send(join);
            while ((line = reader.readLine()) != null) {
                final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
                final JsonElement jsonData = gson.toJsonTree(msgFromServer.data);
                //System.out.println(jsonData);
                //System.out.println("--------------");
                if (msgFromServer.msgType.equals("carPositions")) {
                    if (line.contains("gameTick")) {
                        positionsHandler.handleMessage(jsonData);
                    }
                    //System.out.println("CarPositions:");
                    //positionsHandler.handleMessage(jsonData);
                } else if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                    System.out.println(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    System.out.println("Race init");
                    gameInit = gson.fromJson(jsonData, GameInit.class);
                    trackMetrics = new TrackMetrics(gameInit.race.track);
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    System.out.println("Race end");
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    System.out.println("Race start");
                    send(new Throttle(1.0));
                } else if (msgFromServer.msgType.equals("yourCar")) {
                    System.out.println("yourCar");
                    myCar = gson.fromJson(jsonData, CarID.class);
                } else if (msgFromServer.msgType.equals("crash")) {
                    new CrashMessageHandler().handleMessage(jsonData);
                } else if (msgFromServer.msgType.equals("turboAvailable")) {
                    new TurboMessageHandler().handleMessage(jsonData);
                } else {
                    System.out.println("received unkown message");
                }
            }
            System.out.println("END OF STEAM!");
        } catch (IOException ex) {
            Logger.getLogger(ServerSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    abstract class MessageHandler {

        public abstract void handleMessage(JsonElement jsonData);
    }

    class PositionsMessageHandler extends MessageHandler {

        int skip = 0;
        Double startAngle = null;

        private boolean determinePhysicalConstants() {

            if (physicalConstants.getAcceleration() == null) {
                if (speeds.getSpeed(myCar).speed == 0.0) {
                    send(new Throttle(1.0));//first tick
                } else {
                    physicalConstants.setAcceleration(speeds.getSpeed(myCar).speed);
                    System.out.println("Acceleration = " + physicalConstants.getAcceleration());
                    send(new Throttle(0.0));//second tick
                }
                return true;
            } else if (physicalConstants.getDeceleration() == null) {
                physicalConstants.setDeceleration((physicalConstants.getAcceleration() - speeds.getSpeed(myCar).speed) / physicalConstants.getAcceleration());
                System.out.println("Deceleration = " + physicalConstants.getDeceleration());
                send(new Throttle(1.0));//thrird tick
                return true;
            } else if (physicalConstants.getJousiVoima() == null) {
                return false;
            } else {
                return false;
            }
        }

        private void updatePositions(JsonElement jsonData) {
            currentPositions = gson.fromJson(jsonData, CarPositions.class);
            myCarPosition = currentPositions.getCarPosition(myCar);
            currentPiece = gameInit.race.track.getPiece(myCarPosition.piecePosition.pieceIndex);
            currentLane = gameInit.race.track.getLaneMetrics(myCarPosition.piecePosition.lane.endLaneIndex);
            if (speeds != null) {
                speeds.updateSpeeds(currentPositions);
            } else {
                speeds = new Speeds(trackMetrics, currentPositions);
            }
        }

        private double speedAfterBreakingFor(double distance, double startingSpeed) {
            double currentSpeed = startingSpeed;
            double currentDistance = 0;
            while (currentDistance < distance) {
                currentSpeed -= currentSpeed * physicalConstants.getDeceleration();
                if (currentSpeed < 0.005) {
                    return 0;
                }
                currentDistance += currentSpeed;
            }
            return currentSpeed;
        }

        private boolean canSpeed(CarPosition carPosition, double limit) {
            double speed = speeds.getSpeed(carPosition.id).speed;
            PiecePosition position = carPosition.piecePosition;
            Piece piece = gameInit.race.track.getPiece(position.pieceIndex);
            int nextCurve = gameInit.race.track.getNextCurve(position.pieceIndex);
            int angleCoefficient;
            if (currentPiece.isCurve()) {
                if (currentPiece.angle < 0) {
                    angleCoefficient = 1;
                } else {
                    angleCoefficient = -1;
                }
                //if (Math.abs(myCarPosition.angle) < 55 && speeds.getSpeed(myCar).speed > 6.0) {
                if (speed * speed / (gameInit.race.track.getPiece(nextCurve).radius + currentLane.distanceFromCenter * angleCoefficient) > limit) {//0.46
                    return false;
                } else {
                    return true;
                }
            } else {
                if (gameInit.race.track.getPiece(nextCurve).angle < 0) {
                    angleCoefficient = 1;
                } else {
                    angleCoefficient = -1;
                }
                double distanceToNextCurve = position.distaneToPiece(nextCurve, gameInit.race.track, trackMetrics);
                //if (speeds.getSpeed(myCar).speed-ticksToNextCurve*physicalConstants.getDeceleration() > 7.555) {//7.555
                double acceleration;
                if (turboAvailable != null && turboAvailable.isOn()) {
                    acceleration = physicalConstants.getAcceleration(3.0, speed);
                } else {
                    acceleration = physicalConstants.getAcceleration(1.0, speed);
                }
                double speedAfter = speed + acceleration;
                double arriveSpeed = speedAfterBreakingFor(distanceToNextCurve - speedAfter, speedAfter);
                if (arriveSpeed * arriveSpeed / (gameInit.race.track.getPiece(nextCurve).radius + currentLane.distanceFromCenter * angleCoefficient) > limit) {//0.46
                    return false;
                } else {
                    return true;
                }
            }
        }

        @Override
        public void handleMessage(JsonElement jsonData) {
            updatePositions(jsonData);
            if (determinePhysicalConstants()) {
                return;
            }
            PiecePosition currentPos = myCarPosition.piecePosition;
            Integer nextCurve = gameInit.race.track.getNextCurve(currentPos.pieceIndex);
            double distanceToNextCurve = currentPos.distaneToPiece(nextCurve, gameInit.race.track, trackMetrics);
            ArrayList<CarPosition> carsToCrash = new ArrayList<>();
            for (CarPosition cp : currentPositions) {
                if (cp.id.equals(myCar)) {
                    continue;
                }
                if (cp.piecePosition.lane.startLaneIndex != currentPos.lane.startLaneIndex || cp.piecePosition.lane.endLaneIndex != currentPos.lane.endLaneIndex) {
                    continue;
                }
                if (currentPos.distanceTo(cp.piecePosition, gameInit.race.track, trackMetrics) > 80) {
                    continue;
                }
//                if (canSpeed(cp, 0.1)) {//Might crash
//                    System.out.println("That car is so slow. No way it's gonna crash...");
//                    continue;
//                }
//                if (!canSpeed(cp, 0.70)) {//will crash anyway
//                    System.out.println("will crash anyway");
//                    continue;
//                }
                carsToCrash.add(cp);
            }
            //if (!currentPiece.isCurve() && distanceToNextCurve > 45) {
            //System.out.println("carsTocrash:" + carsToCrash.size());
            if (turboAvailable != null) {
                if (turboAvailable.turboDurationTicks > 0) {
                    if (turboAvailable.activated) {
                        turboAvailable.turboDurationTicks--;
                    }
                    boolean crashAblePieces = false;
                    for (CarPosition cp : carsToCrash) {
                        if (!gameInit.race.track.getPiece(cp.piecePosition.pieceIndex).isCurve()) {
                            if ((canSpeed(cp, 0.50))) {//if it's not gonna crash anyway..
                                crashAblePieces = true;
                                break;
                            } else {
                                System.out.println("The car is gonna crash anyway...");
                            }
                        }
                    }
                    boolean shouldCrash = !currentPiece.isCurve() && distanceToNextCurve > 30 && distanceToNextCurve < 200 && crashAblePieces;
                    if (!turboAvailable.activated && (shouldCrash || (!currentPiece.isCurve() && distanceToNextCurve > 400))) {
                        turboAvailable.activated = true;
                        send(new Turbo("Go faster!!!"));
                        turboAvailable.turboDurationTicks--;
                        return;
                    }
                }
            }
            if (carsToCrash.size() > 0 && turboAvailable != null && turboAvailable.isOn()) {
                send(new Throttle(1.0));
            } else if (canSpeed(myCarPosition, 0.43)) {
                send(new Throttle(1.0));
            } else {
                send(new Throttle(0.0));
            }
        }

    }

    class CrashMessageHandler extends MessageHandler {

        @Override
        public void handleMessage(JsonElement jsonData) {
            System.out.println("Crash!--------------------------------------------------:");
            CarID crashedCar = gson.fromJson(jsonData, CarID.class);
            CarPosition carPos = currentPositions.getCarPosition(crashedCar);
            PiecePosition piecePos = carPos.piecePosition;
            Piece crashPiece = currentPiece;
            double speed = speeds.getSpeed(crashedCar).speed;
            double inPieceDistance = piecePos.inPieceDistance;
            if (crashPiece.isCurve()) {
                double angle = crashPiece.angle;
                double radius = crashPiece.radius;
                System.out.println(speed + " * " + angle + " * " + radius + " * " + inPieceDistance + " = " + speed * angle * radius * inPieceDistance);
            } else {
                System.out.println("Crashed in stragth!");
            }

            //System.out.println("Crash position: "+carPos);
            //System.out.println("Crash piece = "+crashPiece);
            //System.out.println(speeds.getSpeed(crashedCar));
        }

    }

    class TurboMessageHandler extends MessageHandler {

        @Override
        public void handleMessage(JsonElement jsonData) {
            turboAvailable = gson.fromJson(jsonData, TurboAvailable.class);
        }

    }

    void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
