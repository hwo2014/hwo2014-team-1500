/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

/**
 *
 * @author antti
 */
public class Track {
    public String id;
    public String name;
    public Piece[] pieces;//Length / Length+switch / radius+angle
    public LaneMetric[] lanes;
    
    public Piece getPiece(int index) {
        return pieces[index];
    }
    
    public LaneMetric getLaneMetrics(int index) {
        for (LaneMetric laneMetric : lanes) {
            if (laneMetric.index == index) return laneMetric;
        }
        return null;
    }
    
    public Integer getNextCurve(int pieceIndex) {
        for (int i = pieceIndex+1; i <= pieceIndex+pieces.length; i++) {
            Piece piece = pieces[i%pieces.length];
            if (piece.isCurve()) return i%pieces.length;
        }
        return null;
    }
}
