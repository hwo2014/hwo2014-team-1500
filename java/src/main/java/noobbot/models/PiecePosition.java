/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

import com.google.gson.GsonBuilder;
import noobbot.models.util.TrackMetrics;


/**
 *
 * @author antti
 */
public class PiecePosition {
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    
    
    public double distanceTo(PiecePosition target, Track track, TrackMetrics trackMetrics) {
        double targetDist = trackMetrics.distanceFromStart(target.pieceIndex)+target.inPieceDistance;
        double startDist = trackMetrics.distanceFromStart(this.pieceIndex)+inPieceDistance;
        if (targetDist < startDist) { //Starting line between locations
            double distanceToEnd = trackMetrics.trackLength-startDist;
            return distanceToEnd+targetDist;
        } else {
            return targetDist-startDist;
        }
    }
    
    
    public double distaneToPiece(int pieceIndex, Track track, TrackMetrics trackMetrics) {
        double targetDist = trackMetrics.distanceFromStart(pieceIndex);
        double startDist = trackMetrics.distanceFromStart(this.pieceIndex)+inPieceDistance;
        if (targetDist < startDist) { //Starting line between locations
            double distanceToEnd = trackMetrics.trackLength-startDist;
            return distanceToEnd+targetDist;
        } else {
            return targetDist-startDist;
        }
    }
    
    
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
