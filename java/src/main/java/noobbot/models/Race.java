/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

import noobbot.messages.receivedmsg.CarID;

/**
 *
 * @author antti
 */
public class Race {
    public Track track;
    public Car[] cars;
    
    public Car getCar(CarID carID) {
        for (Car car : cars) {
            if (car.id.equals(carID)) {
                return car;
            }
        }
        return null;
    }
}
