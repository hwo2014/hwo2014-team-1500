/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

/**
 *
 * @author antti
 */
public class CarDimensions {

    public double length;
    public double width;
    public double guideFlagPosition;
    
    
    /**
     * Distance from guideFlagPosition to the center of gravity.
     * @return 
     */
    public double getMomenttiRadius() {
        return Math.abs(length/2-guideFlagPosition);
    }
    
}
