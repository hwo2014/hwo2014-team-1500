/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

import com.google.gson.GsonBuilder;
import noobbot.models.CarPosition;

/**
 *
 * @author antti
 */
public class Speed {
    
    public double speed;
    CarPosition lastCarPosition;
    private TrackMetrics trackMetrics;
    
    public Speed(TrackMetrics trackMetrics, CarPosition startPos) {
        this.trackMetrics = trackMetrics;
        speed = 0;
        lastCarPosition = startPos;
    }
    
    public void update(CarPosition carPosition) {
        if (carPosition.piecePosition.pieceIndex == lastCarPosition.piecePosition.pieceIndex) {
            speed = carPosition.piecePosition.inPieceDistance - lastCarPosition.piecePosition.inPieceDistance;
        } else {
            speed = trackMetrics.distanceFromStart(carPosition.piecePosition)-trackMetrics.distanceFromStart(lastCarPosition.piecePosition);
        }
        lastCarPosition = carPosition;
    }
    
//    public void update(CarPosition carPosition) {
//        if (carPosition.piecePosition.pieceIndex == lastCarPosition.piecePosition.pieceIndex) {
//            speed = carPosition.piecePosition.inPieceDistance - lastCarPosition.piecePosition.inPieceDistance;
//        }
//        lastCarPosition = carPosition;
//    }
    
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
