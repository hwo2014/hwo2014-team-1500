/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

/**
 *
 * @author antti
 */
public class PhysicalConstants {
    private Double acceleration = null;
    private Double deceleration = null;
    private Double maxMomentum = null;
    private Double jousiVoima = null;

    public Double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Double acceleration) {
        this.acceleration = acceleration;
    }

    public Double getDeceleration() {
        return deceleration;
    }

    public void setDeceleration(Double deceleration) {
        this.deceleration = deceleration;
    }

    public Double getMaxMomentum() {
        return maxMomentum;
    }

    public void setMaxMomentum(Double maxMomentum) {
        this.maxMomentum = maxMomentum;
    }

    public Double getJousiVoima() {
        return jousiVoima;
    }

    public void setJousiVoima(Double jousiVoima) {
        this.jousiVoima = jousiVoima;
    }
    
    public double getAcceleration(double throttle, double speed) {
        return getAcceleration()*throttle-getDeceleration()*speed;
    }
    

}
