/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

/**
 *
 * @author antti
 */
public class MathUtil {
    
    
    //Keskipakoisvoima
    public static double calculateCentrifugalForce(double curveRadius, double speed) {
        return speed*speed/curveRadius;
    }
    
    public static double calculateRotationForce(double centrifugalForce, double angle) {
        return Math.cos(Math.toRadians(angle))*centrifugalForce;
    }
    
    //Kaarikiihtyvyys
    public static double calculateRotationForce(double curveRadius, double speed, double angle) {
        return calculateRotationForce(calculateCentrifugalForce(curveRadius, speed), angle);
    }
    
    //Angular acceleration
    //Radius is the distance from guide flag to the center of gravity
    public static double calculateRotationMomentum(double curveRadius, double carRadius, double speed, double angle) {
        return calculateRotationForce(curveRadius, speed, angle)*carRadius;
    }
    
    
    /**
     * 
     * @param accelerationacceleration of the car
     * @param slipAngle slip angle of the car
     * @return Acceleration of slipAngle towards 0 angle at the center of gravity
     */
    public static double calculateStraighteningForce(double acceleration, double slipAngle) {
        return Math.sin(Math.toRadians(slipAngle))*acceleration; 
    }
    
    
    /**
     * 
     * @param carRadius distance from guide flag to the center of gravity
     * @param acceleration acceleration of the car
     * @param slipAngle slip angle of the car
     * @return Angular accelera tion of slipAngle towards 0 angle.
     */
    public static double calculateStraighteningMomentum(double carRadius, double acceleration, double slipAngle) {
        return calculateStraighteningForce(acceleration, slipAngle)*carRadius;
    }
    
    
    public static double calculateMaxSpeed(double throttle, PhysicalConstants constants) {
        return throttle*constants.getAcceleration()/constants.getDeceleration();
    }
    
    public static double calculateThrottle(double speed, PhysicalConstants constants) {
        return constants.getDeceleration()*speed/constants.getAcceleration();
    }
    
//    public static double calculateDriftingVelocity(double startAngle, double endAngle, double speed) {
//        return 2*(endAngle-startAngle-speed);
//    }

    
    public static void main(String[] args) {
        PhysicalConstants ph = new PhysicalConstants();
        ph.setAcceleration(0.2);
        ph.setDeceleration(0.02);
        System.out.println(calculateThrottle(5, ph));
        System.out.println(calculateMaxSpeed(0.7, ph));
        System.out.println(calculateThrottle(1, ph));
        double a = calculateRotationMomentum(100, 10, 5.0, 20);
        double b = calculateStraighteningMomentum(10, 0.2, 20);
        System.out.println(a);
        System.out.println(b);
    }
    
}
