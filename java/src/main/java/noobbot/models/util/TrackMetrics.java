/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

import noobbot.models.LaneMetric;
import noobbot.models.Piece;
import noobbot.models.PiecePosition;
import noobbot.models.Track;

/**
 *
 * @author antti
 */
public class TrackMetrics {
    
    private double[] distances;
    public double trackLength;
    private Track track;
    
    
    public TrackMetrics(Track track) {
        this.track = track;
        distances = new double[track.pieces.length];
        Double currentDistance = 0.0;
        for (int pieceIndex = 0; pieceIndex < track.pieces.length; pieceIndex++) {
            distances[pieceIndex] = currentDistance;
            System.out.println(distances[pieceIndex]);
            currentDistance+=track.pieces[pieceIndex].calculateLength();
        }
        trackLength = currentDistance;
    }
    
    public double distanceFromStart(int pieceIndex) {
        return distances[pieceIndex];
    }
    
    public double distanceFromStart(PiecePosition piecePosition) {
        Piece piece = track.getPiece(piecePosition.pieceIndex);
        if (piece.isCurve()) {
            double angleCoefficient;
            if (track.getPiece(piecePosition.pieceIndex).angle < 0) {
                angleCoefficient = 1;
            } else {
                angleCoefficient = -1;
            }
            LaneMetric lane = track.getLaneMetrics(piecePosition.lane.startLaneIndex);
            double distanceFromCenter = lane.distanceFromCenter * angleCoefficient;
            double radius = piece.radius + distanceFromCenter;
            double distanceInRadians = piecePosition.inPieceDistance / radius;
            double distanceFromStartAtMiddle = piece.radius * distanceInRadians;
            return distanceFromStart(piecePosition.pieceIndex) + distanceFromStartAtMiddle;
        } else {
            return distanceFromStart(piecePosition.pieceIndex) + piecePosition.inPieceDistance;
        }
    }

}
