/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import noobbot.messages.receivedmsg.CarID;
import noobbot.messages.receivedmsg.CarPositions;
import noobbot.models.CarPosition;

/**
 *
 * @author antti
 */
public class Speeds {

    ArrayList<Speed> speeds = new ArrayList<>();
    
    public Speeds(TrackMetrics trackMetrics, CarPositions carPositions) {
        for (CarPosition carPos : carPositions) {
            speeds.add(new Speed(trackMetrics, carPos));
        }
    }
    
    
    public Speed getSpeed(CarID carID) {
        for (Speed speed : speeds) {
            if (speed.lastCarPosition.id.equals(carID)) {
                return speed;
            }
        }
        return null;
    }
    
    
    public void updateSpeeds(CarPositions carPositions) {
        for (CarPosition carPosition : carPositions) {
            getSpeed(carPosition.id).update(carPosition);
        }
    }
    
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
