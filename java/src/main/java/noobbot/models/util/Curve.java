/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models.util;

import noobbot.models.CarPosition;
import noobbot.models.Track;

/**
 *
 * @author antti
 */
public class Curve {
    
    public Track track;
    public int[] pieceIndexes;
    
    public Curve(Track track, int... pieceIndexes) {
        this.track = track;
        this.pieceIndexes = pieceIndexes;
    }
    
    
    public boolean testConstantSpeed(double speed) {
        double centrifugalForce = MathUtil.calculateCentrifugalForce(speed, speed);
        
        return false;
    }
    
    
    public boolean testFullThrottle(double speed) {
        return false;
        
    }
    
    public boolean testZeroThrottle(double speed) {
        return false;
    }
    
    
}
