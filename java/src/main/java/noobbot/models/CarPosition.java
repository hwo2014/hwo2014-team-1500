/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

import com.google.gson.GsonBuilder;
import noobbot.messages.receivedmsg.CarID;

/**
 *
 * @author antti
 */
public class CarPosition {
    public CarID id;
    public double angle;
    public PiecePosition piecePosition;
    public int lap;
 
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
    
   
    
    
//    public double distanceTo(CarPosition carPosition, Track track) {
//        return 0;
////        double result = 0;
////        if (carPosition.lap > this.lap) {
////            //forward
////        } else if (carPosition.lap < this.lap) {
////            //backward
////        } else {
////            //Same Lap
////            if (carPosition.piecePosition.pieceIndex > this.piecePosition.pieceIndex) {
////            //forward
////            } else if (carPosition.piecePosition.pieceIndex < this.piecePosition.pieceIndex) {
////            //backward
////            } else {
////                //Same lap and same piece
////                return carPosition.piecePosition.inPieceDistance-this.piecePosition.inPieceDistance;
////            }
////        }
//    }
}
