/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

import com.google.gson.GsonBuilder;

/**
 *
 * @author antti
 */
public class Lane {
    public int startLaneIndex;
    public int endLaneIndex;
    
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
