/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author antti
 */
public class Piece {
    public Double length;
    public Double radius;
    public Double angle;
    @SerializedName("switch")
    public Boolean kytkin;
    
    
    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
    
    
    public double calculateLength(Lane lane, Track track) {
        if (!isCurve()) {
            return length;
        } else {
            double laneDistanceFromCenter;
            if (lane != null && track != null) {
                laneDistanceFromCenter = track.getLaneMetrics(lane.startLaneIndex).distanceFromCenter;
            } else {
                laneDistanceFromCenter = 0;//Just use middle of the road.
            }
            return Math.toRadians(Math.abs(angle)) * (radius - laneDistanceFromCenter);
        }
    }
    
    
    
    
    public double calculateLength() {
        return calculateLength(null, null);
    }
    
    public boolean isCurve() {
        return angle != null;
    }
    
}
